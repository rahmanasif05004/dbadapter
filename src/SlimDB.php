<?php
namespace Asif\SlimDB;

class SlimDB
{
	private static $classInstance;
	private static $host;
	private static $user;
	private static $password;
	private static $database;
	private static $isPersistent;
	private $tableName;
	private $compiledSql;
	private $statement;
	private $whereValues;

	//variables for statements
	private $select;
	private $where;
	private $orWhere;
	private $from;
	private $join;
	private $orderBy;
	private $whereNot;
	private $limit;

	public function __construct($host,$user,$password,$database,$isPersistent=false)
	{
		self::$host=$host;
		self::$user=$user;
		self::$password=$password;
		self::$database=$database;
		self::$isPersistent=$isPersistent;
		
	}

	public static function getInstance()
	{
		if(!self::$classInstance)
		{
			self::$classInstance=new \PDO("mysql:host=".self::$host.";dbname=".self::$database,self::$user,self::$password,[
				\PDO::ATTR_PERSISTENT=>self::$isPersistent
			]);
		}

		return self::$classInstance;
	}

	public function insert($tableName,array $fieldsToBeFilled=[])
	{
		$fieldsName=implode(',',array_keys($fieldsToBeFilled));
		$fieldsValue=array_values($fieldsToBeFilled);
		$placeholders=implode(',',array_fill(0,count($fieldsToBeFilled),'?'));
		$compiledSql="INSERT INTO $tableName(".$fieldsName.") values(".$placeholders.")";
		$statement= self::$classInstance->prepare($compiledSql);
		$statement->execute($fieldsValue);
		return self::$classInstance->lastInsertId();
	}

	public function select($columns='*')
	{
		$this->select='SELECT '.$columns;
		return $this;
	}

	public function selectMax($column)
	{
		$this->select='SELECT MIN('.$column.')';
		return $this;
	}

	public function selectMin($column)
	{
		$this->select='SELECT MAX('.$column.')';
		return $this;
	}

	public function from($tableName)
	{
		$this->from=' FROM '.$tableName;
		return $this;
	}

	public function limit($limit,$offset="")
	{
		$this->limit=' LIMIT '.(int)$limit;
		if($offset!="")
		{
			$this->limit.=' OFFSET '.(int)$offset;
		}
		return $this;
	}

	public function where(array $whereClause=[])
	{
		$whereKeys=array_keys($whereClause);
		$totalWhereKeys=count($whereKeys);
		$this->where.=" WHERE ";
		foreach($whereKeys as $key=>$val)
		{
			if($totalWhereKeys>$key+1)
			{
				$this->where.=$val."=? AND ";
			}
			else
			{
				$this->where.=$val."=?";
			}
		}
		$this->whereValues.=implode(',',array_values($whereClause));
		return $this;
	}

	public function orWhere(array $whereClause=[])
	{
		$whereKeys=array_keys($whereClause);
		$totalWhereKeys=count($whereKeys);
		foreach($whereKeys as $key=>$val)
		{
			$this->orWhere.=" OR ".$val."=?";
		}
		$this->whereValues.=','.implode(',',array_values($whereClause));
		return $this;
	}

	public function join($joinTable,$joinedTablePrimaryKey,$parenttableKey,$joinType='INNER')
	{
		$this->join.=" $joinType JOIN $joinTable ON $joinedTablePrimaryKey=$parenttableKey ";
		return $this;
	}

	public function orderBy(array $orderByParams=[])
	{
		$this->orderBy.=" ORDER BY ".implode(',',array_values($orderByParams));
		return $this;
	}

	public function fetch($fetchType=\PDO::FETCH_ASSOC)
	{
		$this->compiledSql=$this->select.$this->from.$this->where.$this->orWhere.$this->join.$this->orderBy.$this->limit;
		echo $this->compiledSql;
		// $this->statement=self::$classInstance->prepare($this->compiledSql);
		// $this->statement->execute(explode(',',$this->whereValues));
		// return $this->statement->fetchAll($fetchType);
	}
}



$obj=new SlimDB("localhost","root","","herp");
$obj::getInstance();
$obj->selectMax('name')
		->from('test')
		->where(['name'=>'dasdas'])
		->orWhere(['roll'=>14])
		->join('another','another.id','test.id')
		->join('another','another.id','test.id','RIGHT')
		->orderBy(['name','roll'])
		->limit(5,2)
		->fetch();
